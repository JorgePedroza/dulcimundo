/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import java.awt.Color;
import frame.frame;

/**
 *
 * @author halo_
 */
public class CambioDeFrame {
     static frame framePrincipal = new frame();

    public CambioDeFrame() {
    }
    
     public void entradaConLogoDeDosSegundos() {
        /* Este metodo contiene la llamada del frame login llamado fram1
           mostrara una imagen durante 2 segungos y despues de eso,
        destruira el frame1 y recreara el frame2 con los parametros que resive 
        del metodo frame2.
         */
        try {
             framePrincipal.dispose();
             framePrincipal.setLocation(400, 20);
             framePrincipal.setSize(800, 800);
             framePrincipal.setUndecorated(true);
             framePrincipal.setBackground(new Color(0, 0, 0, 0));
             framePrincipal.setVisible(true);
             Thread.sleep(6000);
             framePrincipal.setVisible(false);
             framePrincipal.pack();
             framePrincipal.frameLogin();
        } catch (InterruptedException ex) {
        }
    }

    public void frameDeContenidoGeneral() {
        /*
        Este frame se construye despues de destruir el frame de login 
        y crear el frame de contenido general.
        */
         framePrincipal.frameContenidoGeneral();
    }
    
    public void location(int x, int y){
       framePrincipal.setLocation(x, y);     
    }
    public void  minimzar(){
        
          framePrincipal.repaint();
        framePrincipal.minimizar();
      
    }
 
}
